<?php namespace Liquimedia\Api;

use GuzzleHttp;

class Api
{
    public $baseUrl;
    public $certPath;
    public $pass;
    public $sslKeyPath;
    public $user;
    public $verify;

    public function __construct($baseUrl, $user, $pass, $ssl = []) {
        $this->baseUrl = $baseUrl;
        $this->user = $user;
        $this->pass = $pass;

        $ssl = array_merge(static::DEFAULTS, $ssl);

        $this->certPath = $ssl['certPath'];
        $this->sslKeyPath = $ssl['sslKeyPath'];
        $this->verify = (bool)$ssl['verify'];
    }

    public function getClient($data = null)
    : GuzzleHttp\Client {
        return new GuzzleHttp\Client([
            'auth' => [
                $this->user,
                $this->pass
            ],
            'json' => $data,
            'verify' => $this->verify,
            'cert' => $this->certPath,
            'ssl_key' => $this->sslKeyPath
        ]);
    }

    public function get($path, $query = null)
    : \Psr\Http\Message\ResponseInterface {
        return $this->getClient()->get("$this->baseUrl/$path", [
            "query" => $query
        ]);
    }

    public function getManufacturerBySlug($slug)
    : \Psr\Http\Message\ResponseInterface {
        return $this->get("manufacturers/$slug");
    }

    public function getManufacturers()
    : \Psr\Http\Message\ResponseInterface {
        return $this->get("manufacturers");
    }

    public function getProductBySlug($slug)
    : \Psr\Http\Message\ResponseInterface {
        return $this->get("products/$slug");
    }

    public function getProductsByCategory($category)
    : \Psr\Http\Message\ResponseInterface {
        return $this->get("products", ["category" => $category]);
    }

    public const CATEGORIES = [
        'motorOils',
    ];

    public const DEFAULTS = [
        'certPath' => null,
        'sslKeyPath' => null,
        'verify' => false //$this->certPath
    ];
}